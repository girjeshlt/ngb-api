package com.mppkvvcl.ngbapi.security.repositories;

import com.mppkvvcl.ngbapi.security.beans.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by NITISH on 14-07-2017.
 */
@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    @Query("select role from Role")
    public List<String> findAllRoleNames();
    public Role findByRole(String role);
}
