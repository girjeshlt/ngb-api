package com.mppkvvcl.ngbapi.security;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import org.slf4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by NITISH on 24-04-2017.
 */

public class JWTAuthenticationFilter extends GenericFilterBean {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(JWTAuthenticationFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String methodName = "doFilter() : ";
        logger.info(methodName + "called");
        try{
            Authentication authentication = TokenAuthenticationService.getAuthentication((HttpServletRequest)servletRequest);
            if(authentication != null){
                logger.info(methodName + "Token Verified successfully proceeding with the api request");
                logger.info(methodName + " authentication is " + authentication.getCredentials() + " " + authentication.getDetails());
            }
            SecurityContextHolder.getContext().setAuthentication(authentication);
            filterChain.doFilter(servletRequest,servletResponse);
        }catch (AuthenticationException ae){
            logger.error(methodName + "got AuthenticationException as " + ae.getMessage());
            ((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_UNAUTHORIZED,ae.getMessage());
        }catch (ExpiredJwtException eje){
            logger.error(methodName + "got ExpiredJwtException as " + eje.getMessage());
            ((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_UNAUTHORIZED,eje.getMessage());
        }catch (SignatureException se){
            logger.error(methodName + "got SignatureException as " + se.getMessage());
            ((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_UNAUTHORIZED,se.getMessage());
        }
    }
}
