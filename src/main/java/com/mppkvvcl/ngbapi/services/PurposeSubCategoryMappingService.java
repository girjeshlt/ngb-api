package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PurposeSubCategoryMappingInterface;
import com.mppkvvcl.ngbdao.daos.PurposeSubCategoryMappingDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by NITISH on 26-05-2017.
 */
@Service
public class PurposeSubCategoryMappingService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(PurposeSubCategoryMappingService.class);

    /**
     * Asking Spring to inject PurposeSubCategoryMappingRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on PurposeSubCategoryMapping table
     * at the backend Database
     */
    @Autowired
    private PurposeSubCategoryMappingDAO purposeSubCategoryMappingDAO;

    /**
     * This method uses inject PurposeSubCategoryMappingRepository to give PurposeSubCategoryMapping object based
     * on below parameters<br><br>
     * param purposeOfInstallationId<br>
     * param subcategoryCode<br>
     * return mapping
     */
    public PurposeSubCategoryMappingInterface getByPurposeOfInstallationIdAndSubCategoryCode(long purposeOfInstallationId, long subcategoryCode){
        String methodName = "getByPurposeOfInstallationIdAndSubCategoryCode: ";
        logger.info(methodName + "getting purpose_subcategory_mapping by purposeOfInstallationId: "+purposeOfInstallationId+" subcategoryCode: "+subcategoryCode);
        PurposeSubCategoryMappingInterface mapping = null;
        mapping = purposeSubCategoryMappingDAO.getByPurposeOfInstallationIdAndSubcategoryCode(purposeOfInstallationId,subcategoryCode);
        if(mapping == null) {
            logger.error(methodName + "mapping is null");
        }
        return mapping;
    }
}
