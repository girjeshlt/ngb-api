package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.CTRMasterInterface;
import com.mppkvvcl.ngbdao.daos.CTRMasterDAO;
import com.mppkvvcl.ngbinterface.interfaces.MeterMasterInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ANSHIKA on 04-07-2017.
 */
@Service
public class CTRMasterService {

    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(CTRMasterService.class);

    @Autowired
    private GlobalResources globalResources;

    /**
     * Requesting spring to get singleton CTRMasterRepository object.
     */
    @Autowired
    private CTRMasterDAO ctrMasterDAO;


    /**
     * This insert method takes ctrMaster object and insert it into the CTRMaster table in the backend databae.
     * return insertedCTRObject if successful else return null.<br><br>
     * param ctrMaster<br>
     * return insertedCTRMaster
     */
    public CTRMasterInterface insert(CTRMasterInterface ctrMaster){
        String methodName = "insert() : ";
        CTRMasterInterface insertedCTRMaster = null;
        logger.info(methodName + "Got request to insert ctrMaster");
        if(ctrMaster != null){
            String ctrMake = ctrMaster.getMake();
            String serialNo = ctrMaster.getSerialNo();
            if(ctrMake != null && serialNo != null) {
                String identifier = ctrMake + serialNo;
                ctrMaster.setIdentifier(identifier);
                setAuditDetails(ctrMaster);
                insertedCTRMaster = ctrMasterDAO.add(ctrMaster);
            }
        }
        return insertedCTRMaster;
    }

    /**
     * This method takes make and serialNo from user and fetch CTRMaster object against it.
     * Return retrievedCtrMaster if found else return null.<br><br>
     * param make<br>
     * param serialNo<br>
     * return retrievedCtrMaster
     */
    public CTRMasterInterface getByMakeAndSerialNo(String make, String serialNo) {
        String methodName = "getCTRMasterByMakeAndSerialNo() : ";
        CTRMasterInterface retrievedCtrMaster = null;
        logger.info(methodName + "called " + make + " " + serialNo);
        if (serialNo != null && make != null) {
            String ctrMake = make.trim();
            String ctrSerialNo = serialNo.trim();
            String identifier = ctrMake + ctrSerialNo;
            retrievedCtrMaster = ctrMasterDAO.getByIdentifier(identifier);
        }
        return retrievedCtrMaster;

    }

    /**
     * This method takes identifier and fetch ctrMaster against it
     * Return ctrMaster if successful else return null.<br><br>
     * param identifier<br>
     * return ctrMaster<br>
     */
    public CTRMasterInterface getByIdentifier(String identifier) {
        String methodName = " getByIdentifier() : ";
        logger.info(methodName + "called");
        CTRMasterInterface ctrMaster = null;
        if (identifier != null) {
            ctrMaster = ctrMasterDAO.getByIdentifier(identifier);
        }
        return ctrMaster;
    }

    private void setAuditDetails(CTRMasterInterface ctrMasterInterface){
        final String methodName = "setAuditDetails() : ";
        logger.info(methodName + "called");
        if(ctrMasterInterface != null){
            String user = GlobalResources.getLoggedInUser();
            ctrMasterInterface.setCreatedBy(user + " " + globalResources.getProjectDetail());
            ctrMasterInterface.setCreatedOn(GlobalResources.getCurrentDate());
        }
    }
}
