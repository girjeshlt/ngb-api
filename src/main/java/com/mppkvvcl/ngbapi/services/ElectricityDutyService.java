package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ElectricityDutyInterface;
import com.mppkvvcl.ngbdao.daos.ElectricityDutyDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 10-07-2017.
 */
@Service
public class ElectricityDutyService {
    /**
     * Requesting spring to get singleton ElectricityDutyRepository object.
     */
    @Autowired
    ElectricityDutyDAO electricityDutyDAO;

    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(ElectricityDutyService.class);

    /**
     * This getBySubCategoryCodeAndDate method takes subCategoryCode and Date and fetch the list of electricity duties
     * against it. Return electricityDutyList if successful found else return null.<br><br>
     * param subCategoryCode<br>
     * param givenDate<br>
     * return List of ElectricityDuty<br>
     */
    public List<ElectricityDutyInterface> getBySubCategoryCodeAndDate(Long subCategoryCode, Date givenDate){
        String methodName = "getBySubCategoryCodeAndDate() : ";
        List<ElectricityDutyInterface> electricityDutyList = null;
        logger.info(methodName + "Got request to get electricityDutyList against subCategoryCode : " +subCategoryCode +" and givenDate : "+ givenDate);
        if(subCategoryCode != null && givenDate != null){
            logger.info(methodName + "Calling ElectricityDutyRepository method to fetch electricityDutyList");
            electricityDutyList = electricityDutyDAO.getBySubCategoryCodeAndDate(subCategoryCode, givenDate);
            if(electricityDutyList != null) {
                if (electricityDutyList.size() > 0) {
                    logger.info(methodName + "Got list against subCategoryCode : " +subCategoryCode +" and givenDate : "+ givenDate);
                } else {
                    logger.error(methodName + "No data exists against subCategoryCode : " +subCategoryCode +" and givenDate : "+ givenDate);
                }
            } else{
                logger.error(methodName + "List not found against subCategoryCode : " +subCategoryCode +" and givenDate : "+ givenDate);
            }
        }else{
            logger.error(methodName + "Input param found null");
        }
        return electricityDutyList;
    }

}
