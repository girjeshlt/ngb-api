package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.IrrigationSchemeInterface;
import com.mppkvvcl.ngbdao.daos.IrrigationSchemeDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IrrigationSchemeService {

    Logger logger = GlobalResources.getLogger(IrrigationSchemeService.class);

    @Autowired
    IrrigationSchemeDAO irrigationSchemeDAO;

    public IrrigationSchemeInterface getByConsumerNo(String consumerNo){
        final String methodName = "getByConsumerNo() : ";
        IrrigationSchemeInterface irrigationScheme = null;
        logger.info("called for getting consumer irrigation scheme data for consumer no " + consumerNo);
        irrigationScheme = irrigationSchemeDAO.getByConsumerNo(consumerNo);
        if(irrigationScheme == null){
            logger.info(methodName + "this consumer doesn't have frozen arrear");
        }else {
            logger.info(methodName + "this consumer have frozen arrear amount which is as " +irrigationScheme);
        }
        return irrigationScheme;
    }

}
