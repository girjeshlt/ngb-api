package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PowerFactorInterface;
import com.mppkvvcl.ngbdao.daos.PowerFactorDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.math.BigDecimal;

@Service
public class PowerFactorService {
    /**
     * Asking spring to inject PowerFactorRepository to that we can use it.
     */
    @Autowired
    PowerFactorDAO powerFactorDAO;

    /**
     * Getting logger for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(PowerFactorService.class);

    public PowerFactorInterface getByPfValueAndDate(BigDecimal pfValue, Date date){
        String methodName = "getByPfValueAndDate() : ";
        PowerFactorInterface powerFactor = null;
        logger.info(methodName + "Got request to get Power Factor with range as : " + pfValue +" And date as " + date);
        if(pfValue == null || date ==null){
            logger.error(methodName + " Input paramter range or date null. Please check ");
        }else {
            logger.info(methodName + " Calling power factor repo for fetching power factor");
            powerFactor = powerFactorDAO.getByPowerFactorValueAndDate(pfValue,date);
            if(powerFactor == null){
                logger.error(methodName + " Power Factor couldn't be found for given inputs.");
            }else{
                logger.info(methodName + " Successfully retreived power factor as " + powerFactor);
            }
        }
        return powerFactor;
    }

}
