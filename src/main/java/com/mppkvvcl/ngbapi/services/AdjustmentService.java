package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.custombeans.CustomAdjustment;
import com.mppkvvcl.ngbapi.security.beans.Role;
import com.mppkvvcl.ngbapi.security.services.RoleService;
import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbapi.utility.NGBAPIUtility;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.*;
import com.mppkvvcl.ngbdao.daos.AdjustmentDAO;
import com.mppkvvcl.ngbentity.beans.Adjustment;
import com.mppkvvcl.ngbentity.beans.AdjustmentProfile;
import com.mppkvvcl.ngbentity.beans.UserDetail;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by SHIVANSHU on 14-07-2017.
 */
@Service
public class AdjustmentService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private Logger logger = GlobalResources.getLogger(AdjustmentService.class);

    @Autowired
    private AdjustmentDAO adjustmentDAO;

    @Autowired
    private AdjustmentRangeService adjustmentRangeService;

    @Autowired
    private AdjustmentHierarchyService adjustmentHierarchyService;

    @Autowired
    private AdjustmentProfileService adjustmentProfileService;

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private AdjustmentTypeService adjustmentTypeService;

    /**
     * Asking spring to get Singleton object of LocationService.
     */
    @Autowired
    private ZoneService zoneService;

    /**
     * This getByConsumerNoAndPostingBillMonth is used to get Adjustment object with
     * consumerNo and postingBillMonth.<br><br>
     * param consumerNo<br>
     * param postingBillMonth<br>
     * return adjustment
     */

    //detected wrong on 08012018. Need to correct with frontend changes
    public AdjustmentInterface getByConsumerNoAndPostingBillMonth(String consumerNo, String postingBillMonth) {
        String methodName = "getByConsumerNoAndPostingBillMonth() : ";
        logger.info(methodName + "called");
        AdjustmentInterface adjustment = null;
        if (consumerNo != null && postingBillMonth != null) {
            adjustment = adjustmentDAO.getByConsumerNoAndPostingBillMonth(consumerNo, postingBillMonth);
        }
        return adjustment;
    }

    /**
     * This getByConsumerNoAndPostedAndDeleted method is used for getting list of Adjustment with consumer
     * where posted is false and deleted is also false.<br><br>
     * param consumerNo<br>
     * param posted<br>
     * param deleted<br>
     * return
     */
    public List<AdjustmentInterface> getByConsumerNoAndPostedAndDeleted(String consumerNo, boolean posted, boolean deleted){
        String methodName = "getByConsumerNoAndPostedAndDeleted() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> adjustments = null;
        if (consumerNo != null) {
            adjustments = adjustmentDAO.getByConsumerNoAndPostedAndDeleted(consumerNo, posted, deleted);
        }
        return adjustments;
    }

    public List<AdjustmentInterface> getByConsumerNo(String consumerNo){
        String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> adjustments = null;
        if (consumerNo != null) {
            adjustments = adjustmentDAO.getByConsumerNoAndPostedAndDeleted(consumerNo, AdjustmentInterface.POSTED_FALSE, AdjustmentInterface.DELETED_FALSE);
        }
        return adjustments;
    }

    public AdjustmentInterface getById(long id) {
        String methodName = "getById() : ";
        logger.info(methodName + "called for adjustment id: "+id);
        AdjustmentInterface adjustment = adjustmentDAO.getById(id);
        return adjustment;
    }

    public List<AdjustmentInterface> getAllOpenAdjustmentByLocationCodeAndUserRole(String locationCode, String userRole) throws Exception {
        String methodName = "getAllOpenAdjustmentByLocationCodeAndUserRole() : ";
        logger.info(methodName + "called for location code: "+locationCode);
        List<AdjustmentInterface> adjustments = null;
        ZoneInterface location = null;
        List<ZoneInterface> locations = null;
        if (locationCode == null || userRole == null) {
            logger.error(methodName + "Location Code or Role passed is Null");
            return adjustments;
        }
        switch (userRole) {
            case Role.OAG:
                adjustments = getAllOpenAdjustmentByLocationCode(locationCode);
                break;
            case Role.JE:
                adjustments = getAllOpenAdjustmentByLocationCode(locationCode);
                break;
            case Role.OIC:
                adjustments = getAllOpenAdjustmentByLocationCode(locationCode);
                break;
            case Role.EE:
                location = zoneService.getByLocationCode(locationCode);
                locations = zoneService.getByDivisionId(location.getDivision().getId());
                for(ZoneInterface loc : locations){
                    adjustments.addAll(getAllOpenAdjustmentByLocationCode(loc.getCode()));
                }
                break;
            case Role.SE:
                location = zoneService.getByLocationCode(locationCode);
                locations = zoneService.getByCircleId(location.getDivision().getCircle().getId());
                for(ZoneInterface loc : locations){
                    adjustments.addAll(getAllOpenAdjustmentByLocationCode(loc.getCode()));
                }
                break;
            default:
                logger.error("No Matched user role Found for Role: " + userRole);
                throw new Exception("No Matched user role Found");
        }
        return adjustments;
    }

    public List<AdjustmentInterface> getAllOpenAdjustmentByLocationCode(String locationCode) throws Exception{
        String methodName = "getAllOpenAdjustmentByLocationCode() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> adjustments = null;
        if (locationCode == null) {
            logger.error(methodName + "Location Code passed is Null");
            throw new Exception("Location Code passed is Null");
        }
        adjustments = adjustmentDAO.getByLocationCodeAndPostedAndDeleted(locationCode, AdjustmentInterface.POSTED_FALSE, AdjustmentInterface.DELETED_FALSE);
        return adjustments;
    }

    @Transactional(rollbackFor = Exception.class)
    public AdjustmentInterface insert(AdjustmentInterface adjustment) throws Exception {
        String methodName = "insert() : ";
        logger.info(methodName + "called");

        if (adjustment == null) {
            logger.error("Adjustment object received for insertion is null");
            throw new Exception("Something went wrong. Please try again.");
        }

        List<AdjustmentInterface> adjustmentsAlreadyAdded = adjustmentDAO.getByConsumerNoAndCodeAndPostedAndDeleted(adjustment.getConsumerNo(),adjustment.getCode(), Adjustment.POSTED_FALSE,Adjustment.DELETED_FALSE);
        if(adjustmentsAlreadyAdded != null && adjustmentsAlreadyAdded.size() > 0){
            logger.error("Adjustment Already Added For Adjustment Type "+adjustment.getCode());
            throw new Exception("Adjustment Already Present For provided Adjustment Type for Consumer, Please Check.");
        }

        AdjustmentInterface insertedAdjustment = null;

        String username = GlobalResources.getLoggedInUser();
        UserDetail userDetail = userDetailService.getByUsername(username);
        if (userDetail == null) {
            logger.error("Something wrong happened while getting the userDetail. Received as null");
            throw new Exception("No User Found.");
        }

        String locationCode = userDetail.getLocationCode();
        adjustment.setLocationCode(locationCode);

        Role role = roleService.getByRole(userDetail.getRole());
        if (role == null) {
            logger.error("Could not retrieve role for logged in userDetail");
            throw new Exception("No User Found");
        }

        int userPriority = role.getPriority();

        AdjustmentRangeInterface adjustmentRange = adjustmentRangeService.getAdjustmentRangeByCodeAndAmount(adjustment.getCode(), adjustment.getAmount());
        if (adjustmentRange == null) {
            logger.error("Adjustment Range not found for given adjustment code and amount.");
            throw new Exception("Entered Adjustment Amount is not allowed for this adjustment category.");
        }
        adjustment.setRangeId(adjustmentRange.getId());
        int adjustmentPriority = adjustmentRange.getMaxPriority();
        List<AdjustmentHierarchyInterface> adjustmentHierarchyList = adjustmentHierarchyService.getByUserPriorityAndAdjustmentPriority(userPriority, adjustmentPriority);
        if (adjustmentHierarchyList == null || adjustmentHierarchyList.size() <= 0) {
            logger.error("Adjustment Hierarchy not found.");
            throw new Exception("Unable to add Adjustment.");
        }
        List<AdjustmentProfile> adjustmentProfileList = new ArrayList<AdjustmentProfile>();
        for (AdjustmentHierarchyInterface adjustmentHierarchy : adjustmentHierarchyList) {
            AdjustmentProfile adjustmentProfile = new AdjustmentProfile();
            String roleName = adjustmentHierarchy.getRole();
            if (adjustmentHierarchy.getPriority() == userPriority) {
                adjustmentProfile.setRemark(adjustment.getRemark());
                adjustmentProfile.setApprover(username);
                adjustmentProfile.setStatus(true);
            } else {
                switch (roleName) {
                    case Role.JE:
                        UserDetail jeUserDetail = userDetailService.getByLocationCodeAndRole(locationCode,Role.JE).get(0);
                        adjustmentProfile.setApprover(jeUserDetail.getUsername());
                        break;
                    case Role.OIC:
                        adjustmentProfile.setApprover(userDetailService.getAEByLocationCode(locationCode));
                        break;

                    case Role.EE:
                        adjustmentProfile.setApprover(userDetailService.getEEByLocationCode(locationCode));
                        break;
                    case Role.SE:
                        adjustmentProfile.setApprover(userDetailService.getSEByLocationCode(locationCode));
                        break;
                    default:
                        logger.error("No Matched userDetail role Found for Role: " + roleName);
                        throw new Exception("Incorrect User");
                }
                adjustmentProfile.setStatus(false);
            }
            if (userPriority == adjustmentPriority) {
                adjustment.setApprovalStatus(AdjustmentInterface.APPROVED);
            } else {
                adjustment.setApprovalStatus(AdjustmentInterface.PENDING);
            }
            adjustmentProfile.setLocationCode(locationCode);
            adjustmentProfileList.add(adjustmentProfile);
        }
        setAuditDetails(adjustment);
        insertedAdjustment = adjustmentDAO.add(adjustment);

        if (insertedAdjustment == null) {
            logger.error("Unable to insert adjustment.");
            throw new Exception("Unable to insert adjustment.");
        }
        logger.info("Adjustment inserted Successfully, Proceeding with insert Adjustment Profile.");
        for (AdjustmentProfile profile : adjustmentProfileList) {
            profile.setAdjustmentId(insertedAdjustment.getId());
            adjustmentProfileService.insert(profile);
        }
        return insertedAdjustment;
    }

    @Transactional(rollbackFor = Exception.class)
    public AdjustmentInterface update(AdjustmentInterface adjustment) throws Exception{
        String methodName = "update() : ";
        logger.info(methodName + "called");
        AdjustmentInterface updatedAdjustment = null;
        if (adjustment != null) {
            updatedAdjustment = adjustmentDAO.add(adjustment);
        }
        return updatedAdjustment;
    }

    /**
     * This Method maps List of AdjustmentInterface into List of CustomAdjustments
     * @param adjustments
     * @return CustomAdjustments
     */
    public List<CustomAdjustment> prepareCustomAdjustments(List<? extends AdjustmentInterface> adjustments){
        List<CustomAdjustment> customAdjustments = null;
        if(adjustments != null){
            customAdjustments = new ArrayList<>();
            for(AdjustmentInterface adjustment : adjustments) {
                CustomAdjustment customAdjustment = new CustomAdjustment(adjustment);
                customAdjustment.setAdjustmentType(adjustmentTypeService.getByAdjustmentCode(adjustment.getCode()));
                customAdjustment.setAdjustmentProfiles(adjustmentProfileService.getByAdjustmentId(adjustment.getId()));
                customAdjustments.add(customAdjustment);
            }
        }
        return customAdjustments;
    }

    /**
     * This Method maps AdjustmentInterface into CustomAdjustments
     * @param adjustment
     * @return CustomAdjustment
     */
    public CustomAdjustment prepareCustomAdjustment(AdjustmentInterface adjustment){
        CustomAdjustment customAdjustment = null;
        if(adjustment != null){
            customAdjustment = new CustomAdjustment(adjustment);
            customAdjustment.setAdjustmentType(adjustmentTypeService.getByAdjustmentCode(adjustment.getCode()));
            customAdjustment.setAdjustmentProfiles(adjustmentProfileService.getByAdjustmentId(adjustment.getId()));
        }
        return customAdjustment;
    }

    public List<CustomAdjustment> getByConsumerNoAndApprovalStatus(String consumerNo, String approvalStatus, ErrorMessage errorMessage) {
        String methodName = "getByConsumerNoAndApprovalStatus() : ";
        logger.info(methodName + "called ");

        if (consumerNo == null || approvalStatus == null) {
            errorMessage.setErrorMessage(methodName + "consumer no or status passed is Null");
            return null;
        }

        List<? extends AdjustmentInterface> adjustments = adjustmentDAO.getByConsumerNoAndApprovalStatus(consumerNo,approvalStatus);
        if(adjustments == null || adjustments.size() <= 0){
            errorMessage.setErrorMessage(methodName + "No adjustment found");
            return null;
        }

       List<CustomAdjustment> customAdjustments = prepareCustomAdjustments(adjustments);
        if(customAdjustments == null || customAdjustments.size() != adjustments.size()){
            errorMessage.setErrorMessage(methodName + "error in retrieving custom adjustment");
            return null;
        }
        return customAdjustments;
    }

    //count method
    public long getCountByConsumerNo(String consumerNo){
        final String methodName = "getCountByConsumerNo() : ";
        logger.info(methodName + "called");
        return adjustmentDAO.getCountByConsumerNo(consumerNo);
    }

    public long getCountByLocationCodeAndPostedAndDeleted(String locationCode,boolean posted,boolean deleted){
        final String methodName = "getCountByLocationCode() : ";
        logger.info(methodName + "called");
        return adjustmentDAO.getCountByLocationCodeAndPostedAndDeleted(locationCode,posted,deleted);
    }

    public long getCountByConsumerNoAndApprovalStatus(String consumerNo,String approvalStatus) {
        final String methodName = "getCountByConsumerNoAndApprovalStatus() : ";
        logger.info(methodName + "called");
        return adjustmentDAO.getCountByConsumerNoAndApprovalStatus(consumerNo,approvalStatus);
    }

    public long getAllOpenAdjustmentCountByLocationCodeAndUserRole(String locationCode, String userRole){
        String methodName = "getAllOpenAdjustmentByLocationCodeAndUserRoleCount() : ";
        logger.info(methodName + "called");
        long count = -1;
        if (locationCode != null && userRole != null) {
            count = 0;
            if(userRole.equals(Role.OAG) || userRole.equalsIgnoreCase(Role.JE) || userRole.equalsIgnoreCase(Role.OIC)) {
                count = getAllOpenAdjustmentCountByLocationCode(locationCode);
            }else if(userRole.equals(Role.EE)){
                ZoneInterface location = zoneService.getByLocationCode(locationCode);
                List<ZoneInterface> locations = zoneService.getByDivisionId(location.getDivision().getId());
                for(ZoneInterface zoneInterface : locations){
                    count = count + getAllOpenAdjustmentCountByLocationCode(zoneInterface.getCode());
                }
            }else if(userRole.equals(Role.SE)){
                ZoneInterface location = zoneService.getByLocationCode(locationCode);
                List<ZoneInterface> locations = zoneService.getByCircleId(location.getDivision().getCircle().getId());
                for(ZoneInterface zoneInterface : locations){
                    count = count + getAllOpenAdjustmentCountByLocationCode(zoneInterface.getCode());
                }
            }else {
                count = -1;
            }
        }
        return count;
    }

    public long getAllOpenAdjustmentCountByLocationCode(String locationCode){
        String methodName = "getAllOpenAdjustmentCountByLocationCode() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(locationCode != null){
            count = adjustmentDAO.getCountByLocationCodeAndPostedAndDeleted(locationCode,AdjustmentInterface.POSTED_FALSE,AdjustmentInterface.DELETED_FALSE);
        }
        return count;
    }

    //pageable method
    public List<AdjustmentInterface> getByConsumerNoWithPagination(String consumerNo, String sortBy, String sortOrder, int pageNumber, int pageSize){
        final String methodName = "getByConsumerNoWithPagination() : ";
        logger.info(methodName + "called " + consumerNo + " " + sortBy);
        List<AdjustmentInterface> adjustmentInterfaces = Collections.emptyList();
        pageNumber = pageNumber - 1;
        if(consumerNo != null && sortBy != null && pageNumber >= 0){
            Pageable pageable = null;
            if(sortOrder != null && sortOrder.equalsIgnoreCase(NGBAPIUtility.SORT_ORDER_ASCENDING)){
                if(sortBy.equalsIgnoreCase("postingBillMonth")){
                    pageable = new PageRequest(pageNumber,pageSize, Sort.Direction.ASC,"id");
                    adjustmentInterfaces = adjustmentDAO.getByConsumerNoOrderByPostingBillMonthAscending(consumerNo,pageable);
                }else{
                    pageable = new PageRequest(pageNumber,pageSize, Sort.Direction.ASC,sortBy);;
                    adjustmentInterfaces = adjustmentDAO.getByConsumerNo(consumerNo,pageable);
                }
            }else{
                if(sortBy.equalsIgnoreCase("postingBillMonth")){
                    pageable = new PageRequest(pageNumber,pageSize,Sort.Direction.DESC,"id");
                    adjustmentInterfaces = adjustmentDAO.getByConsumerNoOrderByPostingBillMonthDescending(consumerNo,pageable);
                }else{
                    pageable = new PageRequest(pageNumber,pageSize,Sort.Direction.DESC,sortBy);
                    adjustmentInterfaces = adjustmentDAO.getByConsumerNo(consumerNo,pageable);
                }
            }
        }
        return adjustmentInterfaces;
    }

    public List<AdjustmentInterface> getByLocationCodeAndPostedAndDeletedWithPagination(String locationCode,boolean posted,boolean deleted, String sortBy, String sortOrder, int pageNumber, int pageSize){
        final String methodName = "getByLocationCodeAndPostedAndDeletedWithPagination() : ";
        logger.info(methodName + "called " + locationCode + " " + sortBy);
        List<AdjustmentInterface> adjustmentInterfaces = Collections.emptyList();
        pageNumber = pageNumber - 1;
        if(locationCode != null && sortBy != null && pageNumber >= 0){
            Pageable pageable = null;
            if(sortOrder != null && sortOrder.equalsIgnoreCase(NGBAPIUtility.SORT_ORDER_ASCENDING)){
                if(sortBy.equalsIgnoreCase("postingBillMonth")){
                    pageable = new PageRequest(pageNumber,pageSize, Sort.Direction.ASC,"id");
                    adjustmentInterfaces = adjustmentDAO.getByLocationCodeAndPostedAndDeletedOrderByPostingBillMonthAscending(locationCode,posted,deleted,pageable);
                }else{
                    pageable = new PageRequest(pageNumber,pageSize, Sort.Direction.ASC,sortBy);;
                    adjustmentInterfaces = adjustmentDAO.getByLocationCodeAndPostedAndDeleted(locationCode,posted,deleted,pageable);
                }
            }else{
                if(sortBy.equalsIgnoreCase("postingBillMonth")){
                    pageable = new PageRequest(pageNumber,pageSize,Sort.Direction.DESC,"id");
                    adjustmentInterfaces = adjustmentDAO.getByLocationCodeAndPostedAndDeletedOrderByPostingBillMonthDescending(locationCode,posted,deleted,pageable);
                }else{
                    pageable = new PageRequest(pageNumber,pageSize,Sort.Direction.DESC,sortBy);
                    adjustmentInterfaces = adjustmentDAO.getByLocationCodeAndPostedAndDeleted(locationCode,posted,deleted,pageable);
                }
            }
        }
        return adjustmentInterfaces;
    }

    public List<CustomAdjustment> getByConsumerNoAndApprovalStatusWithPagination(String consumerNo, String approvalStatus,String sortBy,String sortOrder,int pageNumber,int pageSize) {
        String methodName = "getByConsumerNoAndApprovalStatusWithPagination() : ";
        logger.info(methodName + "called ");
        List<AdjustmentInterface> adjustmentInterfaces = Collections.emptyList();
        pageNumber = pageNumber - 1;
        if(consumerNo != null && sortBy != null && pageNumber >= 0){
            Pageable pageable = null;
            if(sortOrder != null && sortOrder.equalsIgnoreCase(NGBAPIUtility.SORT_ORDER_ASCENDING)){
                if(sortBy.equalsIgnoreCase("postingBillMonth")){
                    pageable = new PageRequest(pageNumber,pageSize, Sort.Direction.ASC,"id");
                    adjustmentInterfaces = adjustmentDAO.getByConsumerNoAndApprovalStatusOrderByPostingBillMonthAscending(consumerNo,approvalStatus,pageable);
                }else{
                    pageable = new PageRequest(pageNumber,pageSize, Sort.Direction.ASC,sortBy);;
                    adjustmentInterfaces = adjustmentDAO.getByConsumerNoAndApprovalStatus(consumerNo,approvalStatus,pageable);
                }
            }else{
                if(sortBy.equalsIgnoreCase("postingBillMonth")){
                    pageable = new PageRequest(pageNumber,pageSize,Sort.Direction.DESC,"id");
                    adjustmentInterfaces = adjustmentDAO.getByConsumerNoAndApprovalStatusOrderByPostingBillMonthDescending(consumerNo,approvalStatus,pageable);
                }else{
                    pageable = new PageRequest(pageNumber,pageSize,Sort.Direction.DESC,sortBy);
                    adjustmentInterfaces = adjustmentDAO.getByConsumerNoAndApprovalStatus(consumerNo,approvalStatus,pageable);
                }
            }
        }
        return prepareCustomAdjustments(adjustmentInterfaces);
    }

    public List<AdjustmentInterface> getAllOpenAdjustmentByLocationCodeAndUserRoleWithPagination(String locationCode, String userRole,String sortBy,String sortOrder,int pageNumber,int pageSize){
        String methodName = "getAllOpenAdjustmentByLocationCodeAndUserRoleWithPagination() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> adjustmentInterfaces = Collections.emptyList();
        if (locationCode != null && userRole != null) {
            if(userRole.equals(Role.OAG) || userRole.equalsIgnoreCase(Role.JE) || userRole.equalsIgnoreCase(Role.OIC)) {
                adjustmentInterfaces = getAllOpenAdjustmentByLocationCodeWithPagination(locationCode,sortBy,sortOrder,pageNumber,pageSize);
            }else if(userRole.equals(Role.EE)){
                ZoneInterface location = zoneService.getByLocationCode(locationCode);
                List<ZoneInterface> locations = zoneService.getByDivisionId(location.getDivision().getId());
                for(ZoneInterface zoneInterface : locations){
                    adjustmentInterfaces.addAll(getAllOpenAdjustmentByLocationCodeWithPagination(zoneInterface.getCode(),sortBy,sortOrder,pageNumber,pageSize));
                }
            }else if(userRole.equals(Role.SE)){
                ZoneInterface location = zoneService.getByLocationCode(locationCode);
                List<ZoneInterface> locations = zoneService.getByCircleId(location.getDivision().getCircle().getId());
                for(ZoneInterface zoneInterface : locations){
                    adjustmentInterfaces.addAll(getAllOpenAdjustmentByLocationCodeWithPagination(zoneInterface.getCode(),sortBy,sortOrder,pageNumber,pageSize));
                }
            }
        }
        return adjustmentInterfaces;
    }

    public List<AdjustmentInterface> getAllOpenAdjustmentByLocationCodeWithPagination(String locationCode,String sortBy,String sortOrder,int pageNumber,int pageSize){
        String methodName = "getAllOpenAdjustmentByLocationCodeWithPagination() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> adjustmentInterfaces = Collections.emptyList();
        pageNumber = pageNumber - 1;
        if(locationCode != null && sortBy != null && pageNumber >= 0){
            Pageable pageable = null;
            if(sortOrder != null && sortOrder.equalsIgnoreCase(NGBAPIUtility.SORT_ORDER_ASCENDING)){
                if(sortBy.equalsIgnoreCase("postingBillMonth")){
                    pageable = new PageRequest(pageNumber,pageSize, Sort.Direction.ASC,"id");
                    adjustmentInterfaces = adjustmentDAO.getByLocationCodeAndPostedAndDeletedOrderByPostingBillMonthAscending(locationCode,AdjustmentInterface.POSTED_FALSE,AdjustmentInterface.DELETED_FALSE,pageable);
                }else{
                    pageable = new PageRequest(pageNumber,pageSize, Sort.Direction.ASC,sortBy);;
                    adjustmentInterfaces = adjustmentDAO.getByLocationCodeAndPostedAndDeleted(locationCode,AdjustmentInterface.POSTED_FALSE,AdjustmentInterface.DELETED_FALSE,pageable);
                }
            }else{
                if(sortBy.equalsIgnoreCase("postingBillMonth")){
                    pageable = new PageRequest(pageNumber,pageSize,Sort.Direction.DESC,"id");
                    adjustmentInterfaces = adjustmentDAO.getByLocationCodeAndPostedAndDeletedOrderByPostingBillMonthAscending(locationCode,AdjustmentInterface.POSTED_FALSE,AdjustmentInterface.DELETED_FALSE,pageable);
                }else{
                    pageable = new PageRequest(pageNumber,pageSize,Sort.Direction.DESC,sortBy);
                    adjustmentInterfaces = adjustmentDAO.getByLocationCodeAndPostedAndDeleted(locationCode,AdjustmentInterface.POSTED_FALSE,AdjustmentInterface.DELETED_FALSE,pageable);
                }
            }
        }
        return adjustmentInterfaces;
    }

    private void setAuditDetails(AdjustmentInterface adjustment) {
        String methodName = "setAuditDetails()  : ";
        logger.info(methodName + "called");
        if (adjustment != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            adjustment.setUpdatedOn(date);
            adjustment.setUpdatedBy(user);
            adjustment.setCreatedOn(date);
            adjustment.setCreatedBy(user);
        }
    }
}
