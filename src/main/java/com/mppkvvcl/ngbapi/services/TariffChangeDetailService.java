package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.TariffChangeDetailInterface;
import com.mppkvvcl.ngbdao.daos.TariffChangeDetailDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by vikas on 8/23/2017.
 */

@Service
public class TariffChangeDetailService {
    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(TariffChangeDetailService.class);


    @Autowired
    private TariffChangeDetailDAO tariffChangeDetailDAO;

    public TariffChangeDetailInterface getByTariffDetailId(long tariffDetailId) {
        String methodName = "getByTariffDetailId() :";
        logger.info(methodName + "called "+tariffDetailId);
        TariffChangeDetailInterface tariffChangeDetail = null;
        if(tariffDetailId > 0) {
            tariffChangeDetail = tariffChangeDetailDAO.getByTariffDetailId(tariffDetailId);
        }
        return tariffChangeDetail;
    }

    public TariffChangeDetailInterface insert(TariffChangeDetailInterface tariffChangeDetail) {
        String methodName = "insert() :";
        logger.info(methodName + "called");
        TariffChangeDetailInterface  insertedTariffChangeDetail1 = null;
        setAuditDetails(tariffChangeDetail);
        insertedTariffChangeDetail1 = tariffChangeDetailDAO.add(tariffChangeDetail);
        return  insertedTariffChangeDetail1;
    }

    private void setAuditDetails(TariffChangeDetailInterface tariffChangeDetail){
        String methodName = "setAuditDetails()  :";
        if(tariffChangeDetail != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            tariffChangeDetail.setUpdatedOn(date);
            tariffChangeDetail.setUpdatedBy(user);
            tariffChangeDetail.setCreatedOn(date);
            tariffChangeDetail.setCreatedBy(user);
        }
        else {
            logger.error(methodName + "given input is null");
        }
    }
}
