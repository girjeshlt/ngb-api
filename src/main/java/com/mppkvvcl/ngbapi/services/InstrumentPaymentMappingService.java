package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.InstrumentPaymentMappingInterface;
import com.mppkvvcl.ngbdao.daos.InstrumentPaymentMappingDAO;
import com.mppkvvcl.ngbentity.beans.InstrumentPaymentMapping;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by RUPALI on 14-07-2017.
 */
@Service
public class InstrumentPaymentMappingService {
    /**
     * Requesting spring to get singleton InstrumentPaymentMapping object.
     */
    @Autowired
    InstrumentPaymentMappingDAO instrumentPaymentMappingDAO;
    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(InstrumentPaymentMappingService.class);

    public List<InstrumentPaymentMappingInterface> getByInstrumentDetailId(long instrumentDetailId) {
        String methodName = "getByInstrumentDetailId()  :";
        List<InstrumentPaymentMappingInterface> instrumentPaymentMapping = null;
        logger.info( methodName + "called");
        if(instrumentDetailId > 0) {
            instrumentPaymentMapping = instrumentPaymentMappingDAO.getByInstrumentDetailIdOrderByPaymentIdAsc(instrumentDetailId);
        }
        return instrumentPaymentMapping;
    }

    public InstrumentPaymentMappingInterface getByPaymentId(long paymentId) {
        String methodName = "getByPaymentId()  :";
        InstrumentPaymentMappingInterface instrumentPaymentMapping = null;
        logger.info( methodName + "Got request to get InstrumentPaymentMapping by Given PaymentId"+paymentId);
        if(paymentId > 0){
            instrumentPaymentMapping = instrumentPaymentMappingDAO.getByPaymentId(paymentId);
        }
        return instrumentPaymentMapping;
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean deleteById(long instrumentMappingId) throws  Exception  {
        String methodName = "deleteById() : ";
        boolean instrumentMappingDeleted = true;
        InstrumentPaymentMappingInterface instrumentMappingInDb = instrumentPaymentMappingDAO.getById(instrumentMappingId);
        System.out.println("InstrumnetPayment Mapping Found"+instrumentMappingInDb);
        if (instrumentMappingInDb == null) {
            logger.error(methodName + "instrument  not found");
            throw new Exception("instrument  not found ");
        }

        instrumentPaymentMappingDAO.delete(instrumentMappingInDb);

        return instrumentMappingDeleted;
    }

    @Transactional(rollbackFor = Exception.class)
    public InstrumentPaymentMappingInterface insertByInstrumentIdAndPaymentId(long instrumentId, long paymentId) {
        String methodName = "insertByInstrumentIdAndPaymentId()  :";
        InstrumentPaymentMappingInterface insertedInstrumentPaymentMapping = null;
        logger.info( methodName + "Got request to insert InstrumentPaymentMapping by Given PaymentId "+paymentId+" InstrumentId "+instrumentId);
        if(paymentId > 0 && instrumentId > 0){
            InstrumentPaymentMapping instrumentPaymentMappingToInsert = new InstrumentPaymentMapping();
            instrumentPaymentMappingToInsert.setInstrumentDetailId(instrumentId);
            instrumentPaymentMappingToInsert.setPaymentId(paymentId);
            insertedInstrumentPaymentMapping = instrumentPaymentMappingDAO.add(instrumentPaymentMappingToInsert);

        }else {
            logger.error(methodName + "Given PaymentId or instrument id is null");
        }
        return insertedInstrumentPaymentMapping;
    }
}
