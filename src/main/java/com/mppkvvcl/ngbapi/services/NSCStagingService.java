package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.NSCStagingDAO;
import com.mppkvvcl.ngbentity.beans.NSCStagingStatus;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingInterface;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingStatusInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by NITISH on 31-05-2017.
 * Service class for NSCStaging Table at the backend database to perform
 * various business logic in the application.
 */
@Service
public class NSCStagingService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(NSCStagingService.class);

    @Autowired
    private GlobalResources globalResources;

    /**
     * Asking Spring to inject NSCStagingRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on NSCStaging table
     * at the backend Database
     */
    @Autowired
    private NSCStagingDAO nscStagingDAO;

    @Autowired
    private NSCStagingStatusService nscStagingStatusService;

    /**
     * This insert method takes an NSCStaging class object and insert its data in the
     * backend table of nsc_staging at the database. On successful insertion it returns
     * the newly inserted object with id; if insertion fails it returns null<br><br>
     * param nscStaging<br>
     * return NSCStaging<br>
     */
    public NSCStagingInterface insert(NSCStagingInterface nscStaging){
        String methodName = "insert: ";
        NSCStagingInterface insertedNSCStaging = null;
        logger.info(methodName + "insert nsc staging started");
        if(nscStaging != null){
            setAuditDetails(nscStaging);
            insertedNSCStaging = nscStagingDAO.add(nscStaging);
            if(insertedNSCStaging != null){
                logger.info(methodName + "successfully inserted NSCStaging row");
                NSCStagingStatus nscStagingStatus = new NSCStagingStatus();
                nscStagingStatus.setNscStagingId(insertedNSCStaging.getId());
                nscStagingStatus.setStatus("PENDING");
                NSCStagingStatusInterface insertedNSCStagingStatus = nscStagingStatusService.insert(nscStagingStatus);
                if(insertedNSCStagingStatus == null){
                    insertedNSCStaging = null;
                }
            }
        }
        return insertedNSCStaging;
    }

    /**
     * This getById method takes an id and fetches the NSCStaging object against that id from the backend
     * database using the injected NSCStagingRepository object and returns it if found else returns null<br><br>
     * param id<br>
     * return NSCStaging<br>
     */
    public NSCStagingInterface getById(Long id){
        String methodName = "getById: ";
        logger.info(methodName + " started");
        NSCStagingInterface nscStaging = nscStagingDAO.getById(id);
        return nscStaging;
    }

    private void setAuditDetails(NSCStagingInterface nscStagingInterface){
        final String methodName = "setAuditDetails() : ";
        logger.info(methodName + "called");
        if(nscStagingInterface != null){
            String user = GlobalResources.getLoggedInUser();
            nscStagingInterface.setCreatedBy(user + " " + globalResources.getProjectDetail());
            nscStagingInterface.setCreatedOn(GlobalResources.getCurrentDate());
        }
    }
}
