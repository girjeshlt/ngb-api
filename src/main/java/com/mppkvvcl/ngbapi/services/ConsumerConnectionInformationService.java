package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionInformationInterface;
import com.mppkvvcl.ngbdao.daos.ConsumerConnectionInformationDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by SUMIT on 14-06-2017.
 */
@Service
public class ConsumerConnectionInformationService {
    /**
     * Asking Spring to inject ConsumerConnectionInformationRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on consumer_connection_information_history table
     * at the backend Database
     */
    @Autowired
    private ConsumerConnectionInformationDAO consumerConnectionInformationDAO;

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerConnectionInformationService.class);

    /**
     * This method takes ConsumerConnectionInformation object and insert it into the backend database.
     * Return insertedConsumerConnectionInformation if insertion successful else return null.<br><br>
     * param consumerConnectionInformation<br>
     * return insertedConsumerConnectionInformation
     */
    public ConsumerConnectionInformationInterface insert(ConsumerConnectionInformationInterface consumerConnectionInformation){
        String methodName = "insert: ";
        ConsumerConnectionInformationInterface insertedConsumerConnectionInformation = null ;
        logger.info(methodName + "insert consumer connection information started");
        if(consumerConnectionInformation != null){
            insertedConsumerConnectionInformation = consumerConnectionInformationDAO.add(consumerConnectionInformation);
            if(insertedConsumerConnectionInformation != null){
                logger.info(methodName + "successfully inserted ConsumerConnectionInformation row");
            }else{
                logger.error(methodName + "Insertion of ConsumerConnectionInformation failed");
            }
        }else{
            logger.info(methodName + "ConsumerConnectionInformation to insert is null ");
        }
        return insertedConsumerConnectionInformation;
    }

    /**
     * This method takes consumerNo from user and fetch a consumerConnectionInformation object against it.
     * Return consumerConnectionInformation if successful else return null.<br><br>
     * param consumerNo<br>
     * return consumerConnectionInformation
     */
    public ConsumerConnectionInformationInterface getByConsumerNo(String consumerNo) {
        String methodName = " getByConsumerNo(): ";
        logger.info(methodName + " service method called for fetching Consumer Connection Details by Consumer No: " + consumerNo);
        ConsumerConnectionInformationInterface consumerConnectionInformation = null;
        if (consumerNo != null) {
            consumerNo=consumerNo.trim();
            consumerConnectionInformation = consumerConnectionInformationDAO.getByConsumerNo(consumerNo);
            if (consumerConnectionInformation != null) {
                logger.info(methodName + " fetched consumer connection info:" + consumerNo);
            } else {
                logger.error(methodName + " nothing retrieved for consumer number:" + consumerNo);
            }
        } else {
            logger.error(methodName + "consumerNo passed is null in method.");
        }
        return consumerConnectionInformation;
    }
}
