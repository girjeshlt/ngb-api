package com.mppkvvcl.ngbapi;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootApplication
@EntityScan(basePackages = {"com.mppkvvcl.ngbentity.beans","com.mppkvvcl.ngbapi"})
@EnableJpaRepositories(basePackages = {"com.mppkvvcl.ngbdao.repositories","com.mppkvvcl.ngbapi"})
@ComponentScan(basePackages = {"com.mppkvvcl.ngbdao.daos","com.mppkvvcl.ngbapi"})
public class NgbApiApplication {

	private static final Logger logger = GlobalResources.getLogger(NgbApiApplication.class);

	private static String LOGGER_HOME = System.getenv("LOGGER_HOME");

	private static String PROJECT_LOG_FOLDER = "ngb-api/";

	private static void printEnvironmentVariables(){
		String methodName = "printEnvironmentVariables() : ";
		logger.info(methodName + "Database server url from env is : " + System.getenv("DATABASE_SERVER_IP"));
		logger.info(methodName + "Database server port from env is : " + System.getenv("DATABASE_SERVER_PORT"));
		logger.info(methodName + "Database server schema from env is : " + System.getenv("DATABASE_SERVER_SCHEMA"));
		logger.info(methodName + "Database server username from env is : " + System.getenv("DATABASE_SERVER_USERNAME"));
		logger.info(methodName + "Database server password from env is : " + System.getenv("DATABASE_SERVER_PASSWORD"));
		logger.info(methodName + "Logger Home from env is : " + System.getenv("LOGGER_HOME"));
	}

	private static void setLoggingFile(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
		String loggingFile = LOGGER_HOME + PROJECT_LOG_FOLDER + "NGB-API_" + sdf.format(new Date()) + ".log";
		System.setProperty("logging.file", loggingFile);
	}

	public static void main(String[] args) {
		printEnvironmentVariables();
		setLoggingFile();
		SpringApplication.run(NgbApiApplication.class, args);
	}
}
