package com.mppkvvcl.ngbapi.factories;

import com.mppkvvcl.ngbentity.beans.EmployeeConnectionMapping;
import com.mppkvvcl.ngbinterface.interfaces.EmployeeConnectionMappingInterface;

public class EmployeeConnectionMappingFactory {
    public static EmployeeConnectionMappingInterface build(){
        EmployeeConnectionMappingInterface employeeConnectionMappingInterface = new EmployeeConnectionMapping();
        return employeeConnectionMappingInterface;
    }
}
