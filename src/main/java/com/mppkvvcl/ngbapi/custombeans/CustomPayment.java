package com.mppkvvcl.ngbapi.custombeans;

import com.mppkvvcl.ngbinterface.interfaces.InstrumentDetailInterface;
import com.mppkvvcl.ngbinterface.interfaces.PaymentInterface;
import com.mppkvvcl.ngbentity.beans.InstrumentDetail;
import com.mppkvvcl.ngbentity.beans.Payment;

/**
 * Created by PREETESH on 9/15/2017.
 */
public class CustomPayment {

    boolean instrumentFlag;
    boolean partPaymentFlag;
    boolean agricultureFlag;
    PaymentInterface payment;
    InstrumentDetailInterface instrumentDetail;

    public boolean isInstrumentFlag() {
        return instrumentFlag;
    }

    public void setInstrumentFlag(boolean instrumentFlag) {
        this.instrumentFlag = instrumentFlag;
    }

    public boolean isPartPaymentFlag() {
        return partPaymentFlag;
    }

    public void setPartPaymentFlag(boolean partPaymentFlag) {
        this.partPaymentFlag = partPaymentFlag;
    }

    public InstrumentDetailInterface getInstrumentDetail() {
        return instrumentDetail;
    }

    public void setInstrumentDetail(InstrumentDetail instrumentDetail) {
        if(instrumentDetail != null){
            this.instrumentDetail = instrumentDetail;
        }
    }

    public boolean isAgricultureFlag() {
        return agricultureFlag;
    }

    public void setAgricultureFlag(boolean agricultureFlag) {
        this.agricultureFlag = agricultureFlag;
    }

    public PaymentInterface getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        if(payment != null){
            this.payment = payment;
        }
    }
}
