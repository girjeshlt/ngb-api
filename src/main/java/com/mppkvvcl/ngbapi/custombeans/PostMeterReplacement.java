package com.mppkvvcl.ngbapi.custombeans;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerMeterMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.MeterCTRMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerMeterMapping;
import com.mppkvvcl.ngbentity.beans.MeterCTRMapping;

/**
 * Created by vikas on 31-Oct-17.
 */
public class PostMeterReplacement {

    private ConsumerMeterMappingInterface consumerMeterMapping;

    private MeterCTRMappingInterface meterCTRMapping;

    private CustomReadMaster finalRead;

    private CustomReadMaster startRead;

    private CustomReadMaster currentRead;

    public ConsumerMeterMappingInterface getConsumerMeterMapping() {
        return consumerMeterMapping;
    }

    public void setConsumerMeterMapping(ConsumerMeterMapping consumerMeterMapping) {
        this.consumerMeterMapping = consumerMeterMapping;
    }

    public MeterCTRMappingInterface getMeterCTRMapping() {
        return meterCTRMapping;
    }

    public void setMeterCTRMapping(MeterCTRMapping meterCTRMapping) {
        this.meterCTRMapping = meterCTRMapping;
    }

    public CustomReadMaster getFinalRead() {
        return finalRead;
    }

    public void setFinalRead(CustomReadMaster finalRead) {
        this.finalRead = finalRead;
    }

    public CustomReadMaster getStartRead() {
        return startRead;
    }

    public void setStartRead(CustomReadMaster startRead) {
        this.startRead = startRead;
    }

    public CustomReadMaster getCurrentRead() {
        return currentRead;
    }

    public void setCurrentRead(CustomReadMaster currentRead) {
        this.currentRead = currentRead;
    }

    @Override
    public String toString() {
        return "PostMeterReplacement{" +
                "consumerMeterMapping=" + consumerMeterMapping +
                ", meterCTRMapping=" + meterCTRMapping +
                ", finalRead=" + finalRead +
                ", startRead=" + startRead +
                ", currentRead=" + currentRead +
                '}';
    }
}
