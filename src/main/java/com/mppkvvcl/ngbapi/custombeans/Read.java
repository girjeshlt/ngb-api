package com.mppkvvcl.ngbapi.custombeans;


import com.mppkvvcl.ngbinterface.interfaces.ConsumerInformationInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadTypeConfiguratorInterface;
import com.mppkvvcl.ngbinterface.interfaces.ScheduleInterface;
import com.mppkvvcl.ngbentity.beans.ReadMaster;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.math.BigDecimal;

/**
 * Created by PREETESH on 7/4/2017.
 */

public class Read  {
    private ConsumerInformationInterface consumerInformation;
    private BigDecimal mf;
    private ScheduleInterface schedule;
    private ReadMasterInterface readMaster;
    private ReadTypeConfiguratorInterface readTypeConfigurator;
    private String ctrIdentifier;

    public ConsumerInformationInterface getConsumerInformation() {
        return consumerInformation;
    }

    public void setConsumerInformation(ConsumerInformationInterface consumerInformation) {
        this.consumerInformation = consumerInformation;
    }

    public BigDecimal getMf() {
        return mf;
    }

    public void setMf(BigDecimal mf) {
        this.mf = mf;
    }

    public ScheduleInterface getSchedule() {
        return schedule;
    }

    public void setSchedule(ScheduleInterface schedule) {
        this.schedule = schedule;
    }

    public ReadMasterInterface getReadMaster() {
        return readMaster;
    }

    public void setReadMasterInterface(ReadMasterInterface readMaster) {
        if(readMaster != null) {
            this.readMaster = readMaster;
        }
    }
       public void setReadMaster(ReadMaster readMaster) {
        if(readMaster != null){
            this.readMaster = readMaster;
        }
    }

       public ReadTypeConfiguratorInterface getReadTypeConfigurator() {
        return readTypeConfigurator;
    }

    public void setReadTypeConfigurator(ReadTypeConfiguratorInterface readTypeConfigurator) {
        this.readTypeConfigurator = readTypeConfigurator;
    }

    public String getCtrIdentifier() {
        return ctrIdentifier;
    }

    public void setCtrIdentifier(String ctrIdentifier) {
        this.ctrIdentifier = ctrIdentifier;
    }

    @Override
    public String toString() {
        return "Read{" +
                "consumerInformation=" + consumerInformation +
                ", mf=" + mf +
                ", schedule=" + schedule +
                ", readMaster=" + readMaster +
                ", readTypeConfigurator=" + readTypeConfigurator +
                ", ctrIdentifier='" + ctrIdentifier + '\'' +
                '}';
    }
}