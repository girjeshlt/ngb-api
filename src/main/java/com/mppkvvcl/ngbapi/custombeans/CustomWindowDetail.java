package com.mppkvvcl.ngbapi.custombeans;
import com.mppkvvcl.ngbinterface.interfaces.WindowDetailInterface;
import com.mppkvvcl.ngbentity.beans.UserDetail;

import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 7/25/2017.
 */
public class CustomWindowDetail {

    private List<WindowDetailInterface> windowDetail;

    private Date windowOpenDate;

    private UserDetail userDetail;

    public List<WindowDetailInterface> getWindowDetail() {
        return windowDetail;
    }

    public void setWindowDetail(List<WindowDetailInterface> windowDetail) {
        this.windowDetail = windowDetail;
    }

    public Date getWindowOpenDate() {
        return windowOpenDate;
    }

    public void setWindowOpenDate(Date windowOpenDate) {
        this.windowOpenDate = windowOpenDate;
    }

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    @Override
    public String toString() {
        return "CustomWindowDetail{" +
                "windowDetail=" + windowDetail +
                ", windowOpenDate=" + windowOpenDate +
                ", userDetail=" + userDetail +
                '}';
    }
}
