package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.DivisionService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.DivisionInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by RUPALI on 18-07-2017.
 */
@RestController
@RequestMapping(value = "/division")
public class DivisionController {
    /**
     * Requesting spring to get singleton DivisionController object
     */
    @Autowired
    DivisionService divisionService;
    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(DivisionController.class);

    /**
     * URI - /division<br><br>
     * This getAllDivision method fetch all the divisions when user hit the URI.<br><br>
     * Response 200 for OK status and list of divisions is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 404 for NOT_FOUND status is sent when list is null.<br><br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getAllDivision(){
        String methodName = "getAllDivision() : ";
        List<? extends DivisionInterface> divisions = null;
        ResponseEntity<?> response = null;
        logger.info(methodName + "Got request to view all division : ");
        divisions = divisionService.getAll();
        if(divisions != null){
            if(divisions.size() > 0){
                logger.info(methodName + "List received successfully with rows : " + divisions.size());
                response = new ResponseEntity<>(divisions, HttpStatus.OK);
            }else{
                logger.error(methodName + "No content in the list");
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }else{
            logger.error(methodName + "List not found for division");
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }
}
