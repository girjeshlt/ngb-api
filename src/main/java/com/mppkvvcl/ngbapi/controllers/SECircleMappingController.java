package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.SECircleMappingService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.SECircleMappingInterface;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * coded by nitish on 23-9-2017
 */
@RestController
@RequestMapping("/se/circle/mapping")
public class SECircleMappingController {
    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(SECircleMappingController.class);

    @Autowired
    private SECircleMappingService seCircleMappingService;

    /**
     * code by nitish
     * @param username
     * @return
     */
    @RequestMapping(method = RequestMethod.GET,value = "/username/{username}",produces = "application/json")
    public ResponseEntity<?> getSECircleMappingByUsername(@PathVariable("username")String username){
        final String methodName = "getSECircleMappingByUsername() : ";
        logger.info(methodName + "called for username " + username);
        ResponseEntity<?> response = null;
        SECircleMappingInterface seCircleMapping = null;
        if(username != null){
            seCircleMapping = seCircleMappingService.getByUsername(username);
        }
        if(seCircleMapping != null){
            response = new ResponseEntity<>(seCircleMapping, HttpStatus.OK);
        }else{
            ErrorMessage errorMessage = new ErrorMessage("No SECircleMapping found for username " + username);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }

    /**
     * code by nitish
     * @param circleId
     * @return
     */
    @RequestMapping(method = RequestMethod.GET,value = "/circle/id/{circleId}",produces = "application/json")
    public ResponseEntity<?> getSECircleMappingByCircleId(@PathVariable("circleId")long circleId){
        final String methodName = "getSECircleMappingByCircleId() : ";
        logger.info(methodName + "called for circleId " + circleId);
        ResponseEntity<?> response = null;
        SECircleMappingInterface seCircleMapping = null;
        seCircleMapping = seCircleMappingService.getByCircleId(circleId);
        if(seCircleMapping != null){
            response = new ResponseEntity<>(seCircleMapping, HttpStatus.OK);
        }else{
            ErrorMessage errorMessage = new ErrorMessage("No SECircleMapping found for circleId " + circleId);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
