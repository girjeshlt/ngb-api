package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.OICZoneMappingService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.OICZoneMappingInterface;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * code by nitish on 23-09-2017
 */
@RestController
@RequestMapping("/oic/zone/mapping")
public class OICZoneMappingController {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(OICZoneMappingController.class);

    @Autowired
    private OICZoneMappingService oicZoneMappingService;

    /**
     * code by nitish
     * @param username
     * @return
     */
    @RequestMapping(method = RequestMethod.GET,value = "/username/{username}",produces = "application/json")
    public ResponseEntity<?> getOICZoneMappingByUsername(@PathVariable("username")String username){
        final String methodName = "getOICZoneMappingByUsername() : ";
        logger.info(methodName + "called for username " + username);
        ResponseEntity<?> response = null;
        OICZoneMappingInterface oicZoneMapping = null;
        if(username != null){
            oicZoneMapping = oicZoneMappingService.getByUsername(username);
        }
        if(oicZoneMapping != null){
            response = new ResponseEntity<>(oicZoneMapping, HttpStatus.OK);
        }else{
            ErrorMessage errorMessage = new ErrorMessage("No OICZoneMapping found for username " + username);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }

    /**
     * code by nitish
     * @param zoneId
     * @return
     */
    @RequestMapping(method = RequestMethod.GET,value = "/zone/id/{zoneId}",produces = "application/json")
    public ResponseEntity<?> getOICZoneMappingByZoneId(@PathVariable("zoneId")long zoneId){
        final String methodName = "getOICZoneMappingByZoneId() : ";
        logger.info(methodName + "called for zoneId " + zoneId);
        ResponseEntity<?> response = null;
        OICZoneMappingInterface oicZoneMapping = null;
        oicZoneMapping = oicZoneMappingService.getByZoneId(zoneId);
        if(oicZoneMapping != null){
            response = new ResponseEntity<>(oicZoneMapping, HttpStatus.OK);
        }else{
            ErrorMessage errorMessage = new ErrorMessage("No OICZoneMapping found for zoneId " + zoneId);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
