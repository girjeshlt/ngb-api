package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.custombeans.CustomCashWindow;
import com.mppkvvcl.ngbapi.exceptionhandlers.RuntimeExceptionHandler;
import com.mppkvvcl.ngbapi.services.BillService;
import com.mppkvvcl.ngbapi.services.CashWindowStatusService;
import com.mppkvvcl.ngbapi.services.PaymentService;
import com.mppkvvcl.ngbapi.services.WindowDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.CashWindowStatusInterface;
import com.mppkvvcl.ngbinterface.interfaces.PaymentInterface;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbentity.beans.WindowDetail;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by PREETESH on 7/25/2017.
 */
@RestController
@RequestMapping(value = "cash-window")
public class CashWindowStatusController {

    /**
     * Getting logger object from GlobalResources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(CashWindowStatusController.class);

    /**
     * Requesting Spring to inject singleton cashWindowStatusService object.
     */
    @Autowired
    CashWindowStatusService cashWindowStatusService;

    /**
     * Requesting Spring to inject singleton windowDetailService object.
     */
    @Autowired
    WindowDetailService windowDetailService;

    @Autowired
    BillService billService;

    @Autowired
    PaymentService paymentService;


    /**
     * URI  /cash-window/window/<br><br>
     * This method is executed when user hit the URI.<br><br>
     * Response 200 for OK status and list of Windows available on location.<br>
     * Response 208 ALREADY_REPORTED status is sent if an OPEN window found for user.<br>
     * Response 417 for EXPECTATION FAILED status if no window available for new cashier.<br><br>
     * return response
     */
    @RequestMapping(value = "/status", method = RequestMethod.GET , produces = "application/json")
    public ResponseEntity<?> getWindowStatus(){
        String methodName = "getWindowStatus() : ";
        CustomCashWindow customCashWindow = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage =null;
        logger.info(methodName + "Got request to get cash window ");
        String loggedInUser = GlobalResources.getLoggedInUser();
        logger.info(methodName + "logged in user is "+loggedInUser);
        List<CashWindowStatusInterface> cashWindowStatuses = cashWindowStatusService.getByUsernameAndStatus(loggedInUser,CashWindowStatusInterface.STATUS_OPEN);
        if(cashWindowStatuses != null && cashWindowStatuses.size() == 1 ) {
            CashWindowStatusInterface cashWindowStatus = cashWindowStatuses.get(0);
            if(cashWindowStatus != null){
                customCashWindow = cashWindowStatusService.getCustomCashWindow(cashWindowStatus);
                if (customCashWindow != null){
                    logger.info(methodName + "found an open window against user " + loggedInUser + " window: " + cashWindowStatus);
                    response = new ResponseEntity<>(customCashWindow, HttpStatus.ALREADY_REPORTED);
                }else{
                    logger.error(methodName + "some error in creating custom bean");
                    errorMessage = new ErrorMessage("found open window, but some error in creating custom bean");
                    response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
                }
            }else{
                logger.error(methodName + "cash window found null in list");
                errorMessage = new ErrorMessage("error in retrieving open cash window ");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else {
            logger.info(methodName + "no open windows found ");
            errorMessage = new ErrorMessage("no open windows found");
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        return  response;
    }


    /**
     * Added By: Preetesh Date: 11 Aug 2017
     * @return
     */
    @RequestMapping(value = "/status/open/all", method = RequestMethod.GET , produces = "application/json")
    public ResponseEntity<?> getAllOpenWindow() {
        String methodName = "getAllOpenWindow() : ";
        List<CustomCashWindow> customCashWindows ;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        logger.info(methodName + "Got request to get status of windows of the location of loggedIn user ");
        try{
            customCashWindows = cashWindowStatusService.getAllWindowsDetails();
        }catch(RuntimeException e){
            errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
            logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
            customCashWindows = null;
        }catch (Exception ee){
            errorMessage = new ErrorMessage(ee.getMessage());
            logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
            customCashWindows = null;
        }

        if (customCashWindows != null) {
            logger.info(methodName + "found windows for location of user "+customCashWindows);
            if(customCashWindows.size() > 0){
                logger.info(methodName + "open windows found ");
                response = new ResponseEntity<>(customCashWindows, HttpStatus.OK);
            }else{
                logger.error(methodName + "no open window found ");
                errorMessage = new ErrorMessage("no open window found ");
                response = new ResponseEntity<>(errorMessage, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
            }

        } else {
            logger.error(methodName + "some error in creating custom bean");
            errorMessage = new ErrorMessage("No Assigned open window found ");
            response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }


    /**
     * Added By: Preetesh Date: 11 Aug 2017
     * @return
     */
    @RequestMapping(value = "/payments", method = RequestMethod.GET , produces = "application/json")
    public ResponseEntity<?> getAllPaymentsOfOpenWindow() {
        String methodName = "getAllPaymentsForOpenWindow() : ";
        List<PaymentInterface> payments ;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        logger.info(methodName + "Got request to get all payments received at window of loggedIn user ");
        try{
            payments = cashWindowStatusService.getAllPaymentsOfOpenWindow();
        }catch(RuntimeException e){
            errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
            logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
            payments = null;
        }catch (Exception ee){
            errorMessage = new ErrorMessage(ee.getMessage());
            logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
            payments = null;
        }

        if (payments != null) {
            logger.info(methodName + "found payments "+payments);
            if(payments.size() > 0){
                logger.info(methodName + "payments found ");
                response = new ResponseEntity<>(payments, HttpStatus.OK);
            }else{
                logger.error(methodName + "no payments found ");
                errorMessage = new ErrorMessage("no payments found");
                response = new ResponseEntity<>(errorMessage, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
            }
        } else {
            logger.error(methodName + "window not found");
            response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }



    /**
     * URI for closing  window
     * @param freezeOnDate
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT,value = "/close/window-name/{windowName}/date/{freezeOnDate}", produces = "application/json")
    public ResponseEntity closeWindow(@PathVariable String windowName,@PathVariable String freezeOnDate) {
        String methodName = "closeWindow(): ";
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage =null;
       CashWindowStatusInterface updatedCashWindowStatusInterface= null;
        logger.info(methodName + "  requests to close window: ");
        if (freezeOnDate != null && windowName != null) {
            try{
                updatedCashWindowStatusInterface = cashWindowStatusService.closeWindow(windowName,freezeOnDate);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                updatedCashWindowStatusInterface = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                updatedCashWindowStatusInterface = null;
            }
            if (updatedCashWindowStatusInterface != null) {
                logger.info(methodName + " successfully closed window at controller");
                response = new ResponseEntity<>(updatedCashWindowStatusInterface,HttpStatus.OK);
            } else {
                logger.error(methodName + " some error in closing window, Sending exception object so that client may fulfill decision requirements.");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }

        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from closeWindow");
        return response;
    }


    /**
     * URI for freezing new/ backdated window
     *
     * @param windowDetail
     * @param userName
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/open/user/{userName}/date/{openDate}", consumes = "application/json", produces = "application/json")
    public ResponseEntity openWindow(@RequestBody WindowDetail windowDetail,
                                     @PathVariable String userName,
                                     @PathVariable String openDate) {
        String methodName = "openWindow(): ";
        ResponseEntity<?> response = null;
        CustomCashWindow customCashWindow = null;
        ErrorMessage errorMessage = null;
        logger.info(methodName + "  requests to freeze window: ");
        if (userName != null && windowDetail != null) {
            try {
                customCashWindow = cashWindowStatusService.openCashWindow(windowDetail, userName, openDate);
            } catch (RuntimeException e) {
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                customCashWindow = null;
            } catch (Exception ee) {
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                customCashWindow = null;
            }
            if (customCashWindow != null) {
                logger.info(methodName + " successfully freezed window at controller");
                response = new ResponseEntity<CustomCashWindow>(customCashWindow, HttpStatus.CREATED);
            } else {
                logger.error(methodName + " some error freezing window, Sending exception object so that client may fulfill decision requirements.");
                response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
            }

        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from openWindow");
        return response;
    }
}
