package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.custombeans.CustomTariffDetail;
import com.mppkvvcl.ngbapi.exceptionhandlers.RuntimeExceptionHandler;
import com.mppkvvcl.ngbapi.services.TariffDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.TariffDetailInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by ANSHIKA on 27-07-2017.
 */
@RestController
@RequestMapping(value = "/tariff/detail")
public class TariffDetailController {

    /**
     * Requesting spring to get singleton tariffDetailService object.
     */
    @Autowired
    TariffDetailService tariffDetailService;

    /**
     * Getting whole logger object from GlobalResources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(TariffDetailController.class);

    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity getTariffDetailByConsumerNo(@PathVariable("consumerNo") String consumerNo) {
        String methodName = "getTariffDetailByConsumerNo() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        CustomTariffDetail customTariffDetail = null;
        ErrorMessage errorMessage = null;
        if (consumerNo != null) {
            try{
                customTariffDetail = tariffDetailService.getByConsumerNo(consumerNo);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                customTariffDetail = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage()  );
                customTariffDetail = null;
            }
            if(customTariffDetail != null){
                logger.info(methodName + "tariff detail set successfully ");
                response =  new ResponseEntity<>(customTariffDetail, HttpStatus.OK);
            }else{
                logger.error(methodName + "some error in fetching tariff detail data ");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.POST,consumes = "application/json", produces = "application/json")
    public ResponseEntity insertTariffDetail(@RequestBody CustomTariffDetail customTariffDetail) {
        String methodName = "insertTariffDetail(): ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        TariffDetailInterface insertedTariffDetail = null;
        ErrorMessage errorMessage =null;
        if (customTariffDetail != null) {
                try{
                    insertedTariffDetail = tariffDetailService.insertTariffChange(customTariffDetail);
                }catch(RuntimeException e){
                    errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                    logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                    insertedTariffDetail = null;
                }catch (Exception ee){
                    errorMessage = new ErrorMessage(ee.getMessage());
                    logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                    insertedTariffDetail = null;
                }
                if (insertedTariffDetail != null) {
                    logger.info(methodName + " updated tariff details in controller");
                    response = new ResponseEntity<>(insertedTariffDetail, HttpStatus.CREATED);
                } else {
                    logger.error(methodName + " some error in insertion. Sending exception object so that client may fulfill decision requirements.");
                    response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
                }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from insert tariff details");
        return response;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/custom/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity getLatestCustomTariffByConsumerNo(@PathVariable("consumerNo") String consumerNo) {
        String methodName = "getLatestCustomTariffByConsumerNo() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        CustomTariffDetail customTariffDetail = null;
        ErrorMessage errorMessage = null;
        if (consumerNo != null) {
            try{
                customTariffDetail = tariffDetailService.getLatestCustomTariffByConsumerNo(consumerNo);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                customTariffDetail = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage()  );
                customTariffDetail = null;
            }
            if(customTariffDetail != null){
                logger.info(methodName + "tariff detail set successfully ");
                response =  new ResponseEntity<>(customTariffDetail, HttpStatus.OK);
            }else{
                logger.error(methodName + "some error in fetching tariff detail data ");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from fetch tariff detail");
        return response;
    }
}
