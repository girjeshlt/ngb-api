package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.exceptionhandlers.RuntimeExceptionHandler;
import com.mppkvvcl.ngbapi.services.EmployeeConnectionMappingService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.EmployeeConnectionMapping;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.EmployeeConnectionMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by vikas on 08-Nov-17.
 */

@RestController
@RequestMapping(value = "/employee/connection/mapping")
public class EmployeeConnectionMappingController {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(EmployeeConnectionMappingController.class);

    @Autowired
    private EmployeeConnectionMappingService employeeConnectionMappingService;

    @RequestMapping(method = RequestMethod.GET,value ="/consumer-no/{consumerNo}/status/{status}",produces ="application/json")
    public ResponseEntity getByConsumerNoAndStatus(@PathVariable("consumerNo") String consumerNo,@PathVariable("status") String status){
        String methodName = "getByConsumerNoAndStatus() : ";
        logger.info(methodName + "called "+consumerNo);
        List<EmployeeConnectionMappingInterface> employeeConnectionMappings = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        if(consumerNo != null && status != null) {
            employeeConnectionMappings = employeeConnectionMappingService.getByConsumerNoAndStatus(consumerNo,status);
            if (employeeConnectionMappings != null && employeeConnectionMappings.size() > 0) {
                response = new ResponseEntity<>(employeeConnectionMappings, HttpStatus.OK);
            } else {
                errorMessage = new ErrorMessage("No Employee Connection Mapping exist for consumer no: " + consumerNo+ " And status: "+status);
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else {
            logger.error(methodName + "Got input parameter is  null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET,value ="/employee-no/{employeeNo}/status/{status}",produces ="application/json")
    public ResponseEntity getByEmployeeNoAndStatus(@PathVariable("employeeNo") String employeeNo,@PathVariable("status") String status){
        String methodName = "getByEmployeeNoAndStatus() : ";
        logger.info(methodName + "called "+employeeNo);
        List<EmployeeConnectionMappingInterface> employeeConnectionMappings = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        if(employeeNo != null && status != null) {
            employeeConnectionMappings = employeeConnectionMappingService.getByEmployeeNoAndStatus(employeeNo,status);
            if (employeeConnectionMappings != null && employeeConnectionMappings.size() > 0) {
                response = new ResponseEntity<>(employeeConnectionMappings, HttpStatus.OK);
            } else {
                errorMessage = new ErrorMessage("No Employee Connection Mapping exist for employee no: " + employeeNo+" And status: "+status);
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else {
            logger.error(methodName + "inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
