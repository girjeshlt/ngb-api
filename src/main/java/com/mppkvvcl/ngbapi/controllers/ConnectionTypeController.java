package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.ConnectionTypeService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConnectionTypeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.GeneratedValue;
import java.util.List;

/**
 * Created by ANSHIKA on 18-07-2017.
 */
@RestController
@RequestMapping(value = "/connection-type")
public class ConnectionTypeController {
    /**
     * Requesting spring to get singleton ConnectionTypeService object.
     */
    @Autowired
    ConnectionTypeService connectionTypeService;

    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(ConnectionTypeController.class);

    /**
     * URI - /connection-type<br><br>
     * This getAllZone method fetches all the connection-type when user hit the URI.<br><br>
     * Response 200 for OK status and list is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 404 for NOT_FOUND status is sent when list is null.<br><br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getAll(){
        String methodName = "getAll() : ";
        List<? extends ConnectionTypeInterface> connectionTypes = null;
        ResponseEntity<?> response = null;
        logger.info(methodName + "Got request to view all connectionTypes");
        connectionTypes = connectionTypeService.getAll();
        if(connectionTypes != null) {
            if (connectionTypes.size() > 0) {
                logger.info(methodName + "List received successfully with rows : " + connectionTypes.size());
                response = new ResponseEntity<>(connectionTypes, HttpStatus.OK);
            } else {
                logger.error(methodName + "No content found in list");
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }else {
            logger.error(methodName + "List not found");
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }
}
