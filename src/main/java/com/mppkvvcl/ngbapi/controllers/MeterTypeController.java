package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.MeterTypeService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterTypeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by SHIVANSHU on 04-07-2017.
 * EDITED By nitish on 25072017
 * changed root path to '/meter/type' earlier was '/meter-description' which is not correct
 */
@RestController
@RequestMapping(value = "/meter/type")
public class MeterTypeController {

    /*
        Getting Logger object for logging in current class MeterTypeController from GlobalResources
     */
    private Logger logger = GlobalResources.getLogger(MeterTypeController.class);

    /**
     * For asking spring to inject singleton object of MeterTypeService  so that we can perform
     * various operations through repo
     */
    @Autowired
    private MeterTypeService meterTypeService;

    /**
     * URL : /meter-type<br><br>
     * This getAll method used for getting all MeterType objects for
     * URL : meter-type/<br><br>
     * Response : OK Status- 200 : If MeterType objects' list found successfully<br>
     * Response : NOT_FOUND Status- 400 : If there is no object inside list<br>
     * Response : EXPECTATION_FAILED Status- 417 : If list return Null<br><br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET , produces = "application/json")
    public ResponseEntity<?> getAll(){
        String methodName = "getAll() :";
        ResponseEntity<?> response = null;
        logger.info(methodName+"    Starting to get all MeterType objects list");
        List<? extends MeterTypeInterface> meterTypes = meterTypeService.getAll();
        if (meterTypes != null){
            if (meterTypes.size() > 0){
                logger.info(methodName+"    MeterType objects' list found successfully");
                response = new ResponseEntity<Object>(meterTypes , HttpStatus.OK);
            }else{
                logger.error(methodName+"   MeterType objects' list found but there is no object inside it");
                response = new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
            }

        }else{
            logger.error(methodName+"   MeterType objects' list return NULL");
            response = new ResponseEntity<Object>(HttpStatus.EXPECTATION_FAILED);
        }
    return response;
    }

    /**
     * URI - /meter/type//phase/{phase}
     * This method takes meterPhase and fetch the list of meterType against it from backend database when user hit the URI.
     * Returns 200 for OK status and meterType list if successful.<br>
     * Returns 204 for NO_CONTENT status when no content in list  .<br>
     * Returns 404 for NOT_FOUND  when list not found.<br>
     * Returns 400 for BAD_REQUEST  when input param is null.<br><br>
     * param meterPhase
     * return response
     */
    @RequestMapping(method = RequestMethod.GET, value = "/phase/{meterPhase}", produces = "application/json")
    public ResponseEntity<?> getByMeterPhase(@PathVariable("meterPhase") String meterPhase){
        String methodName = "getByMeterPhase() : ";
        logger.info(methodName + "Got request to view list of meterRent from MeterRent table");
        List<? extends MeterTypeInterface> meterTypes = null;
        ResponseEntity<?> response = null;
        if(meterPhase != null){
            logger.info(methodName + "Calling meterTypeService method to get list of meterType");
            meterTypes = meterTypeService.getByMeterPhase(meterPhase);
            if(meterTypes != null){
                if(meterTypes.size() > 0){
                    logger.info(methodName + "List of MeterType against meterPhase : " + meterPhase + "received successfully");
                    response = new ResponseEntity<>(meterTypes, HttpStatus.OK);
                }else{
                    logger.error(methodName + "No content found against meterPhase : " + meterPhase);
                    response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
                }
            }else{
                logger.error(methodName + "List of MeterType not found against meterPhase : " + meterPhase);
                response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }else{
            logger.error(methodName + "It was a BadRequest : input param meterPhase : " + meterPhase + "found null ");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return  response;
    }
}
