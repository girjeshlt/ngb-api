package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.CTRMasterService;
import com.mppkvvcl.ngbapi.services.MeterCTRMappingService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.CTRMasterInterface;
import com.mppkvvcl.ngbinterface.interfaces.MeterCTRMappingInterface;
import com.mppkvvcl.ngbentity.beans.MeterCTRMapping;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by VIKAS on 7/5/2017.
 */

@RestController
@RequestMapping(value = "/meter/ctr/mapping")
public class MeterCTRMappingController {

    private static final Logger logger = GlobalResources.getLogger(MeterCTRMapping.class);
    @Autowired
    private CTRMasterService ctrMasterService;

    @Autowired
    MeterCTRMappingService meterCTRMappingService;

    /**
     * URI - /meter/ctr/mapping/identifier/{identifier}/status/{status}<br><br>
     * This method takes identifier and status and fetches list of meterCTRMapping when user hit the URI.<br><br>
     * Response 200 for OK status and list of meterCTRMapping is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 417 EXPECTATION_FAILED is sent if list is null.<br>
     * Response 400 for BAD_REQUEST status is sent when input param found null.<br><br>
     * param identifier<br>
     * param status<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET,value = "/identifier/{identifier}/status/{status}",produces = "application/json")
    public ResponseEntity getByIdentifierAndStatus(@PathVariable("identifier")String identifier, @PathVariable("status") String status){
        String methodName = " getByIdentifierAndStatus() :  ";
        CTRMasterInterface ctrMaster = null;
        List <MeterCTRMappingInterface> meterCTRMappings =null;
        ResponseEntity<?> response = null;
        logger.info(methodName + "Got request to get MeterCTRMapping  Details by CTRIdentifier and Status :  " + identifier );
        if(identifier != null){
            ctrMaster = ctrMasterService.getByIdentifier(identifier.trim());
            if(ctrMaster != null && identifier !=null && status != null ){
                logger.info(methodName+"calling ctrmapping service with "+ctrMaster + " "+identifier+" " +status);
                meterCTRMappings = meterCTRMappingService.getByCTRIdentifierAndMappingStatus(identifier,status);
                if (meterCTRMappings != null && meterCTRMappings.size() > 0){
                    logger.info(methodName+"record fetched"+meterCTRMappings);
                    response = new ResponseEntity<List<MeterCTRMappingInterface>>(meterCTRMappings,HttpStatus.OK);
                }else {
                    logger.info(methodName + "Got zero rows");
                    response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
                }
            }else{
                logger.error(methodName + "CTR Not Found,returning Expectation Failed as response");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else {
            logger.error(methodName + "Input paremeter passed does not meet requirement of being NOT NULL, hence returning BAD REQUEST as response");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return  response;
    }


}
