package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.AgricultureBill6MonthlyService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.AgricultureBill6MonthlyInterface;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ANKIT on 15-09-2017.
 */
@RestController
@RequestMapping(value = "agriculture-6monthly-bill")
public class AgricultureBill6MonthlyController {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(AgricultureBill6MonthlyController.class);

    /*
    Asking spring to inject AgricultureBill6MonthlyService object so that we can use
    its methods for performing various operations through repo.
     */
    @Autowired
    private AgricultureBill6MonthlyService agricultureBill6MonthlyService;

    /**
     * URI : agriculture-6monthly-bill/latest/consumer-no/?<br><br>
     * This getLatestBillByConsumerNo method takes consumerNo to fetch Agriculture Bill with URI. <br><br>
     * Response: 200(Ok) For fetch successful Agriculture Bill<br>
     * Response: 417(Expectation_failed): For fetch null Agriculture Bill<br>
     * Response: 400(Bad_Request): For received consumerNo in parameter is null.<br><br>
     * param consumerNo<br>
     * return response
     */

    @RequestMapping(method = RequestMethod.GET, value = "/latest/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity getLatestAgriculture6MonthlyBillByConsumerNo(@PathVariable("consumerNo") String consumerNo) {
        String methodName = "getLatestAgriculture6MonthlyBillByConsumerNo() : ";
        logger.info(methodName + " Got requests to fetch Latest 6 monthly agriculture Bill by consumer no , " + consumerNo);
        ErrorMessage errorMessage = null;
        AgricultureBill6MonthlyInterface bill = null;
        ResponseEntity<?> response = null;
        if (consumerNo == null) {
            errorMessage = new ErrorMessage("Invalid Consumer No. Please Check.");
            logger.error(methodName + " Consumer No Null. Returning bad request");
            response = new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
            return response;
        }
        try{
            bill = agricultureBill6MonthlyService.getTopByConsumerNo(consumerNo);
        }catch(RuntimeException e){
            errorMessage = new ErrorMessage("Internal Error Occurred.");
            logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            return response;
        }catch (Exception exception){
            errorMessage = new ErrorMessage("Unable to fetch the Bill. Something Went Wrong.");
            logger.error(methodName + "Received Runtime Exception in controller with error message as :" + exception.getMessage());
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            return response;
        }
        if (bill == null) {
            errorMessage = new ErrorMessage("No Agriculture Bill Available For Consumer.");
            logger.error(methodName + " No Agriculture Bill Retrieved for Consumer no "+consumerNo);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            return response;
        }
        logger.info(methodName + " Got Agriculture Bill in controller.");
        response = new ResponseEntity<>(bill, HttpStatus.OK);
        logger.info(" Returning Agriculture Bill from "+methodName);
        return response;
    }
}
