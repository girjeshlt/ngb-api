package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.MeteringTypeService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeteringTypeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by SHIVANSHU on 18-07-2017.
 * Edited by NITISH on 25-07-2017
 * changed root path to '/metering/type' earlier was 'metering-type' which was wrong
 */
@RestController
@RequestMapping(value = "/metering/type")
public class MeteringTypeController {
    /**
     * Asking spring to get Singleton object of MeteringTypeService.
     */
    @Autowired
    MeteringTypeService meteringTypeService;
    /**
     * Getting logger for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(MeteringTypeController.class);

    /**
     * URI : /metering-type<br><br>
     * This method is used for getting all MeteringType object with URI : /metering-type<br><br>
     * Response : 200(Ok) for successfully found the list of MeteringType<br>
     * Response : 417(EXPECTATION_FAILED) for facing problem to get MeteringType list<br><br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET , produces =  "application/json")
    public ResponseEntity<?> getAll(){
        String methodName = "getAll() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        List<? extends MeteringTypeInterface> meteringTypes = meteringTypeService.getAll();
        if (meteringTypes != null){
            if(meteringTypes.size() > 0 ){
                logger.info(methodName+"MeteringType list found successfully ");
                response = new ResponseEntity<>(meteringTypes , HttpStatus.OK);
            }else{
                logger.error(methodName+"MeteringTyepe list found with size"+meteringTypes.size());
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName+"MeteringType list return null");
            response = new ResponseEntity<Object>(HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
