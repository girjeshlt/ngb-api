package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.custombeans.CustomLoadDetail;
import com.mppkvvcl.ngbapi.exceptionhandlers.RuntimeExceptionHandler;
import com.mppkvvcl.ngbapi.services.LoadDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.LoadDetailInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "load")
public class LoadDetailController {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(LoadDetailController.class);

    /*
    Asking spring to inject loadDetailService object so that we can use
    its methods for performing various operations through repo.
     */
    @Autowired
    private LoadDetailService loadDetailService;

    @RequestMapping(method = RequestMethod.GET,value = "/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity<?> getLatestLoadDetail(@PathVariable("consumerNo") String consumerNo){
        String methodName = "getLatestLoadDetail()";
        logger.info(methodName + " Got request to fetch latest load detail of the consumer with consumer no : " + consumerNo);
        ResponseEntity<?> response = null;
        LoadDetailInterface loadDetail = null;
        if (consumerNo != null){
            logger.info(methodName + " Calling LoadDetailService to fetch load detail. ");
            loadDetail = loadDetailService.getLatestLoadDetail(consumerNo);
            if(loadDetail != null){
                response = new ResponseEntity<>(loadDetail,HttpStatus.OK);
            }else{
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else {
            logger.error(methodName + "Got input parameter consumerNo as null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET,value = "/custom/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity<?> getCustomLoadDetailByConsumerNo(@PathVariable("consumerNo") String consumerNo){
        String methodName = "getCustomLoadDetailByConsumerNo() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        CustomLoadDetail customLoadDetail = null;
        ErrorMessage errorMessage = null;
        if (consumerNo != null) {
            try{
                customLoadDetail = loadDetailService.getCustomLoadDetailByConsumerNo(consumerNo);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                customLoadDetail = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage()  );
                customLoadDetail = null;
            }
            if(customLoadDetail != null){
                logger.info(methodName + "Custom load detail set successfully ");
                response =  new ResponseEntity<>(customLoadDetail, HttpStatus.OK);
            }else{
                logger.error(methodName + "some error in fetching load detail data ");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from fetch load detail");
        return response;
    }

    @RequestMapping(method = RequestMethod.POST,value = "change/sub-category/code/{code}",consumes = "application/json",produces = "application/json")
    public ResponseEntity insert(@RequestBody CustomLoadDetail customLoadToInsert, @PathVariable("code") long subCategoryCodeToUpdate){
        String methodName="insert() : ";
        logger.info(methodName +"called");
        ResponseEntity<?> response = null;
        CustomLoadDetail insertedCustomLoadDetail = null;
        ErrorMessage errorMessage = null;
        if(customLoadToInsert != null ){
            try {
                insertedCustomLoadDetail = loadDetailService.insertLoadChange(customLoadToInsert,subCategoryCodeToUpdate);
            }catch (RuntimeException re){
                errorMessage = new ErrorMessage("Some internal error. Try after some time ");
                response = new ResponseEntity<Object>(errorMessage, HttpStatus.EXPECTATION_FAILED);
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
            if(insertedCustomLoadDetail != null){
                logger.info(methodName+": Load Detail data saved");
                response = new ResponseEntity<>(insertedCustomLoadDetail,HttpStatus.CREATED);
            }else{
                logger.error(methodName+": Load Detail error . insertedLoadDetail may be NULL ");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName+": load detail is NULL");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
